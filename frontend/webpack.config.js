var webpack = require('webpack');
var path = require('path');

module.exports = {
    entry: "./app/js/entry.js",
    output: {
        path: path.join(__dirname, './dist'),
        filename: "bundle.js"
    },
    module: {
        //loaders: [
        //    // **IMPORTANT** This is needed so that each bootstrap js file required by
        //    // bootstrap-webpack has access to the jQuery object
        //    { test: /bootstrap\/js\//, loader: 'imports?jQuery=jquery' },
        //
        //    // Needed for the css-loader when [bootstrap-webpack](https://github.com/bline/bootstrap-webpack)
        //    // loads bootstrap's css.
        //    { test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,   loader: "url?limit=10000&minetype=application/font-woff" },
        //    { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&minetype=application/octet-stream" },
        //    { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,    loader: "file" },
        //    { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&minetype=image/svg+xml" }
        //]
    },
    resolve: {
        alias: {
            jquery: "jquery/dist/jquery.js",
            _: "underscore/underscore.js",
            Backbone: "backbone/backbone.js",
            'Backbone.LocalStorage': "backbone.localStorage/backbone.localStorage.js",
            'Backbone.routefilter': 'backbone.routefilter/dist/backbone.routefilter.js'
            // 'scribe': './scribe-master/src/scribe.js'
        },
        root: 'bower_components'
    },
    plugins: [
        new webpack.DefinePlugin({
            ENTER_KEY: 13,
            ESC_KEY: 27
        }),
        new webpack.ProvidePlugin({
            '$': 'jquery',
            '_': '_',
            'Backbone': 'Backbone',
            'Backbone.LocalStorage': 'Backbone.LocalStorage'
          })
    ]
};
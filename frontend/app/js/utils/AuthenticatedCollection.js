/**
 * Created by abhinav on 26/1/15.
 */
//REFERENCE: https://blog.dylants.com/2014/02/13/authenticated-model-a-strategy-for-secure-apis-in-backbone/
/**
 * Wrap Backbone's error with our own, which handles unauthenticated response codes
 * and performs the necessary logic in this case (navigate to login page, perhaps)
 *
 * @param  {Object} options The options for the sync function
 */
function wrapBackboneError(options) {
    var error = options.error;
    options.error = function(response) {
        if (response.status === 401) {
            // Add logic to send the user to the login page,
            // or general authentication page.
            //
            // In this example, we'll send the user to the "login" page:
            Backbone.history.navigate("login", {
                trigger: true
            });
        } else {
            if (error) error(response);
        }
    };
}

// Extend Backbone's Collection to override the sync function. Doing so allows us
// to get a hook into how the errors are handled. Here we can check if the
// response code is unauthorized, and if so, navigate to the login page
module.exports = Backbone.Collection.extend({
    sync: function(method, model, options) {
        wrapBackboneError(options);
        Backbone.Model.prototype.sync.apply(this, arguments);
    }
});
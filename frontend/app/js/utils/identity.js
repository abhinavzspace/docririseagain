/**
 * Created by abhinav on 26/1/15.
 */
var common = require('./common.js');

module.exports = {
    isIdentityResolved: function () {
        return window.localStorage.getItem('sf.identity');
    },
    isInRole: function (role, identity) {

        if (!identity.roles) {
            return false;
        }

        return identity.roles.indexOf(role) !== -1;
    },
    isInAnyRole: function (roles) {
        var identity;
        var identityString = window.localStorage.getItem('sf.identity');
        if (identityString !== null) {
            identity = JSON.parse(identityString);
            for (var i = 0; i < roles.length; i++) {
                if (this.isInRole(roles[i], identity)) {
                    return true;
                }
            }
        }
        return false;
    },
    authenticate: function (identity) {
        if (identity) {
            window.localStorage.setItem('sf.identity', JSON.stringify(identity));
        } else {
            window.localStorage.removeItem('sf.identity');
        }
    },
    getIdentity: function () {
        return JSON.parse(window.localStorage.getItem('sf.identity'));
    }
};
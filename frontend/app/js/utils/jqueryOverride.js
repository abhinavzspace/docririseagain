/**
 * Created by abhinav on 26/1/15.
 */
var identityContainer = require('./identity.js');

module.exports = function() {
    //REFERENCE: http://stackoverflow.com/a/19149076
    $(document).ajaxSend(function(event, request) {
        if (identityContainer.isIdentityResolved()) {
            var identity = identityContainer.getIdentity();
            console.log('identity : ', identity);
            if (identity !== null && identity.token !== null) {
                request.setRequestHeader("x-auth-token", identity.token);
                request.setRequestHeader("x-tenant-id", identity.tenantId);
            }
        }
    });
};

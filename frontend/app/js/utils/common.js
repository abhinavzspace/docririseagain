module.exports = {
    rolesToUrls: {
        'User': ['home', '', 'signin'],
        'Admin': ['home', '', 'signin', 'restricted']
    },
    urlToRoles: {
        '': ['User', 'Admin'],
        'home': ['User', 'Admin'],
        'signin': ['User', 'Admin'],
        'restricted': ['Admin'],
        'accessdenied': ['User', 'Admin']
    },
    returnToState: false
};

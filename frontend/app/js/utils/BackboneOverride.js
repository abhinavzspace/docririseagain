/**
 * Created by abhinav on 26/1/15.
 */
/**
 *
 */

var identityContainer = require('./identity.js');

module.exports = function() {
    // Store "old" sync function
    var backboneSync = Backbone.sync;

    Backbone.sync = function (method, model, options) {

        /*
         * "options" represents the options passed to the underlying $.ajax call
         */
        if (identityContainer.isIdentityResolved()) {
            var identity = identityContainer.getIdentity();
            console.log('identity : ', identity);
            if (identity !== null && identity.token !== null) {
                options.headers = {
                    'x-auth-token': identity.token,
                    'x-tenant-id': identity.tenantId
                }
            }
        }

        // call the original function
        backboneSync(method, model, options);
    };
};

'use strict';

var identityContainer = require('./utils/identity.js');
var common = require('./utils/common.js');
var HomeView = require('./views/home.js');
var SigninView = require('./views/signin.js');
var RestrictedView = require('./views/restricted.js');
var AccessdeniedView = require('./views/accessdenied.js');
var homeView = new HomeView;
var signinView = new SigninView;
var restrictedView = new RestrictedView;
var accessdeniedView = new AccessdeniedView;

var Router = Backbone.Router.extend({
	routes: {
		'': 'redirectToHome',
		'home': 'home',
		'signin': 'signin',
		'restricted': 'restricted',
		'accessdenied': 'accessdenied'
	},
	//REFERENCE: https://github.com/abhinavzspace/backbone.routefilter
	//return false if you dont want to process current url further.
	//before: function(route, params) {
	//	console.log(route);
    //
	//	if (route === '') {
	//		if (route !== 'home') { // to stop looping
	//			Backbone.history.navigate('home', true);
	//			return false;
	//		}
	//	} else {
    //
	//		if (route === 'signin') {
	//			return true;
	//		}
    //
	//		if (!identityContainer.isIdentityResolved()) {
	//			Backbone.history.navigate('signin', true);
	//			return false;
	//		} else {
	//			// check if "route user wants to go to" matches available mappings of "url to roles"
	//			// check if any of the roles which the user has, map to the roles inside this route
	//			if (_.has(common.urlToRoles, route) && common.urlToRoles[route] &&
	//				common.urlToRoles[route].length > 0 && identityContainer.isInAnyRole(common.urlToRoles[route])) {
	//				return true
    //
	//			} else {
	//				if (route !== 'accessdenied') { // to stop looping
	//					Backbone.history.navigate('accessdenied', true);
	//					return false;
	//				}
	//			}
	//		}
	//	}
    //
    //
	//},
	home: function () {
		// for asynchronous loading
		//require.ensure([], function() {
		//	var HomeView = require('./views/home.js');
		//	var homeView = new HomeView;
		//	homeView.render();
		//});
		homeView.render();
	},
	signin: function() {
		signinView.render();
	},
	restricted: function() {
		restrictedView.render();
	},
	accessdenied: function() {
		accessdeniedView.render();
	}
});

module.exports = Router;

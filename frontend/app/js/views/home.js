var homeTemplate = require('ejs!../../templates/home.ejs');
var identityContainer = require('../utils/identity.js');

var Scribe = require('../../../components/scribe-master/src/scribe.js');
var scribePluginBlockquoteCommand = require('scribe-plugin-blockquote-command/scribe-plugin-blockquote-command.js');
var scribePluginCodeCommand = require('scribe-plugin-code-command/scribe-plugin-code-command.js');

// var scribePluginCurlyQuotes = require('scribe-plugin-curly-quotes/scribe-plugin-curly-quotes.js');
var scribePluginCurlyQuotes = require('../../../components/scribe-plugin-curly-quotes/src/scribe-plugin-curly-quotes.js');

var scribePluginFormatterPlainTextConvertNewLinesToHtml = require('scribe-plugin-formatter-plain-text-convert-new-lines-to-html/scribe-plugin-formatter-plain-text-convert-new-lines-to-html.js');
var scribePluginHeadingCommand = require('scribe-plugin-heading-command/scribe-plugin-heading-command.js');
var scribePluginIntelligentUnlinkCommand = require('scribe-plugin-intelligent-unlink-command/scribe-plugin-intelligent-unlink-command.js');

// var scribePluginKeyboardShortcuts = require('scribe-plugin-keyboard-shortcuts/scribe-plugin-keyboard-shortcuts.js');
var scribePluginKeyboardShortcuts = require('../../../components/scribe-plugin-keyboard-shortcuts/src/scribe-plugin-keyboard-shortcuts.js');

var scribePluginLinkPromptCommand = require('scribe-plugin-link-prompt-command/scribe-plugin-link-prompt-command.js');

// var scribePluginSanitizer = require('scribe-plugin-sanitizer/scribe-plugin-sanitizer.js');
var scribePluginSanitizer = require('../../../components/scribe-plugin-sanitizer/src/scribe-plugin-sanitizer.js');

var scribePluginSmartLists = require('scribe-plugin-smart-lists/scribe-plugin-smart-lists.js');
var scribePluginToolbar = require('scribe-plugin-toolbar/scribe-plugin-toolbar.js');

module.exports = Backbone.View.extend({
    el: '#content',
    events: {
        'click #signout': 'signout'
    },
    render: function() {
        this.$el.html(homeTemplate);


        var scribe = new Scribe(document.querySelector('.scribe'), { allowBlockElements: true });

          scribe.on('content-changed', updateHTML);

          function updateHTML() {
            document.querySelector('.scribe-html').value = scribe.getHTML();
          }

          /**
           * Keyboard shortcuts
           */

          var ctrlKey = function (event) { return event.metaKey || event.ctrlKey; };

          var commandsToKeyboardShortcutsMap = Object.freeze({
            bold: function (event) { return event.metaKey && event.keyCode === 66; }, // b
            italic: function (event) { return event.metaKey && event.keyCode === 73; }, // i
            strikeThrough: function (event) { return event.altKey && event.shiftKey && event.keyCode === 83; }, // s
            removeFormat: function (event) { return event.altKey && event.shiftKey && event.keyCode === 65; }, // a
            linkPrompt: function (event) { return event.metaKey && ! event.shiftKey && event.keyCode === 75; }, // k
            unlink: function (event) { return event.metaKey && event.shiftKey && event.keyCode === 75; }, // k,
            insertUnorderedList: function (event) { return event.altKey && event.shiftKey && event.keyCode === 66; }, // b
            insertOrderedList: function (event) { return event.altKey && event.shiftKey && event.keyCode === 78; }, // n
            blockquote: function (event) { return event.altKey && event.shiftKey && event.keyCode === 87; }, // w
            code: function (event) { return event.metaKey && event.shiftKey && event.keyCode === 76; }, // l
            h2: function (event) { return ctrlKey(event) && event.keyCode === 50; } // 2
          });

          /**
           * Plugins
           */

          scribe.use(scribePluginBlockquoteCommand());
          scribe.use(scribePluginCodeCommand());
          scribe.use(scribePluginHeadingCommand(2));
          scribe.use(scribePluginIntelligentUnlinkCommand());
          scribe.use(scribePluginLinkPromptCommand());
          scribe.use(scribePluginToolbar(document.querySelector('.toolbar')));
          scribe.use(scribePluginSmartLists());
          scribe.use(scribePluginCurlyQuotes());
          scribe.use(scribePluginKeyboardShortcuts(commandsToKeyboardShortcutsMap));

          // Formatters
          scribe.use(scribePluginSanitizer({
            tags: {
              p: {},
              br: {},
              b: {},
              strong: {},
              i: {},
              strike: {},
              blockquote: {},
              code: {},
              ol: {},
              ul: {},
              li: {},
              a: { href: true },
              h2: {},
              u: {}
            }
          }));
          scribe.use(scribePluginFormatterPlainTextConvertNewLinesToHtml());

          if (scribe.allowsBlockElements()) {
            scribe.setContent('<p>Hello, World!</p>');
          } else {
            scribe.setContent('Hello, World!');
          }


        return this;
    },
    signout: function(e) {
        e.preventDefault();
        identityContainer.authenticate(null);
        Backbone.history.navigate('signin', true);
    }
});


var restrictedTemplate = require('ejs!../../templates/restricted.ejs');
module.exports = Backbone.View.extend({
    el: '#content',
    render: function() {
        this.$el.html(restrictedTemplate);
        return this;
    }
});
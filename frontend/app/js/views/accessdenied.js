
var deniedTemplate = require('ejs!../../templates/denied.ejs');
module.exports = Backbone.View.extend({
    el: '#content',
    render: function() {
        this.$el.html(deniedTemplate);
        return this;
    }
});
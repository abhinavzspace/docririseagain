var signinTemplate = require('ejs!../../templates/signin.ejs');
var identityContainer = require('../utils/identity.js');
var common = require('../utils/common.js');

module.exports = Backbone.View.extend({
    el: '#content',
    events: {
        'submit form': 'submit'
    },
    render: function() {
        this.$el.html(signinTemplate);
        return this;
    },
    submit : function(e) {
        e.preventDefault();
        var email = this.$el.find('input[name="email"]').val();
        var password = this.$el.find('input[name="password"]').val();
        //return $.ajax({
        //    dataType: 'json',
        //    contentType:"application/json; charset=utf-8",
        //    method: 'POST',
        //    url: 'login',
        //    data: JSON.stringify({
        //        email: email,
        //        password: password
        //    }),
        //    success: function (data) {
        //        var identity = {
        //            token: data.token,
        //            tenantId: data.tenantId,
        //            roles: ['User']
        //        };
        //        identityContainer.authenticate(identity);
        //
        //        if (common.returnToState) {
        //            //return to the saved state before signin
        //            //Backbone.history.navigate(common.returnToState.name, common.returnToState.params);
        //            Backbone.history.navigate(common.returnToState.name, true);
        //        } else {
        //            Backbone.history.navigate('home', true);
        //        }
        //    }
        //});
        Backbone.history.navigate('home', true);
    }
});

'use strict';
//require('css!../css/styles.css');
//var backboneOverride = require('./utils/BackboneOverride.js');

//REFERENCE: https://github.com/bline/bootstrap-webpack
//require("bootstrap-webpack");

var jqueryOverride = require('./utils/jqueryOverride.js');
var Router = require('./router.js');

//REFERENCE: https://github.com/abhinavzspace/backbone.routefilter
require('Backbone.routefilter');
//backboneOverride not needed i guess, as jqueryOverride will also make backboneOverride.
//backboneOverride();

jqueryOverride();

new Router();
Backbone.history.start();
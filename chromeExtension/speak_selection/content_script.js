/**
 * Copyright (c) 2011 The Chromium Authors. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */





var speakKeyStr;

function speakSelection() {
  var focused = document.activeElement;
  var selectedText;
  if (focused) {
    try {
      selectedText = focused.value.substring(
          focused.selectionStart, focused.selectionEnd);
    } catch (err) {
    }
  }
  if (selectedText == undefined) {
    var sel = window.getSelection();
    var selectedText = sel.toString();
  }
  console.log(selectedText);
  console.log("focused : ", focused);
  chrome.extension.sendRequest({'speak': selectedText});
}

function onExtensionMessage(request) {
  if (request['speakSelection'] != undefined) {
    if (!document.hasFocus()) {
      return;
    }
    speakSelection();
  } else if (request['key'] != undefined) {
    speakKeyStr = request['key'];
  }
}

function initContentScript() {
  chrome.extension.onRequest.addListener(onExtensionMessage);
  chrome.extension.sendRequest({'init': true}, onExtensionMessage);

  document.addEventListener('keydown', function(evt) {
    if (!document.hasFocus()) {
      return true;
    }
    var keyStr = keyEventToString(evt);
    if (keyStr == speakKeyStr && speakKeyStr.length > 0) {
      speakSelection();
      evt.stopPropagation();
      evt.preventDefault();
      return false;
    }
    return true;
  }, false);
}


function initTextColor() {
  document.onmouseup = function(e) {
    if (window.getSelection) {
        var allNodes, i, len, node, startNode, endNode;
        var sel = window.getSelection();
        if (sel.toString() === '') {
            return true;
        }

        var span = document.createElement("span");
        span.style.backgroundColor = "yellow";
        if (sel.rangeCount) {
            var range = sel.getRangeAt(0).cloneRange();
            console.log(getNodesInRange(range));
            // TODO : implement startNode and endNode
            allNodes = getNodesInRange(range);
            wrap.call(span, allNodes);
            // range.surroundContents(span);
            // sel.removeAllRanges();
            // sel.addRange(range);
        }
    }
  };
}


// CUSTOM_START : http://stackoverflow.com/a/7931003/1019748
function getNextNode(node)
{
    if (node.firstChild)
        return node.firstChild;
    while (node)
    {
        if (node.nextSibling)
            return node.nextSibling;
        node = node.parentNode;
    }
}

function getNodesInRange(range)
{
    var start = range.startContainer;
    var end = range.endContainer;
    var commonAncestor = range.commonAncestorContainer;
    var nodes = [];
    var node;

    // walk parent nodes from start to common ancestor
    for (node = start.parentNode; node; node = node.parentNode)
    {
        // nodes.push(node);
        if (node == commonAncestor)
            break;
    }
    // nodes.reverse();

    // walk children and siblings from start until end is found
    for (node = start; node; node = getNextNode(node))
    {
        if (node == start && node == end) break;
        if (node == start) continue;
        if (node == end) break;
        nodes.push(node);
    }

    return nodes;
}

//CUSTOM_END : http://stackoverflow.com/a/7931003/1019748


// http://stackoverflow.com/a/13169465/1019748
// Wrap an HTMLElement around each element in an HTMLElement array.
function wrap(elms) {
    // Convert `elms` to an array, if necessary.
    if (!elms.length) elms = [elms];
    
    // Loops backwards to prevent having to clone the wrapper on the
    // first element (see `child` below).
    for (var i = elms.length - 1; i >= 0; i--) {
        var child = (i > 0) ? this.cloneNode(true) : this;
        var el    = elms[i];
        
        // Cache the current parent and sibling.
        var parent  = el.parentNode;
        var sibling = el.nextSibling;
        
        // Wrap the element (is automatically removed from its current
        // parent).
        child.appendChild(el);
        
        // If the element had a sibling, insert the wrapper before
        // the sibling to maintain the HTML structure; otherwise, just
        // append it to the parent.
        if (sibling) {
            parent.insertBefore(child, sibling);
        } else {
            parent.appendChild(child);
        }
    }
};



initTextColor();
initContentScript();

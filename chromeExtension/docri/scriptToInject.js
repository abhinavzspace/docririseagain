// get a new smallest number from the NUMBERS array
	function getNewNumber() {
		if (NUMBERS.length === 0) {
			NUMBERS.push(1);
			return 1;
		} else {
			var i, len, prev;
			NUMBERS.sort();
			prev = NUMBERS[0];
			if (prev !== 1) {
				NUMBERS.push(1);
				return 1;
			}
			for (i = 1, len = NUMBERS.length; i < len; i++) {
				if ((prev + 1) < NUMBERS[i]) {
					NUMBERS.push(prev + 1);
					return (prev + 1);
				} else {
					prev = NUMBERS[i];
				}
			}
			NUMBERS.push(prev + 1);
			return (prev + 1);
		}
	}

	//remove an element by value from an array
	function removeValueFromArray(arr) {
	    var what, a = arguments, L = a.length, ax;
	    while (L > 1 && arr.length) {
	        what = a[--L];
	        while ((ax= arr.indexOf(what)) !== -1) {
	            arr.splice(ax, 1);
	        }
	    }
	    return arr;
	}

	function createContainer(number) {
		var a = document.createElement('a');
		a.style.zIndex = '99999999';
		a.style.display = 'inline-block';
		a.style.visibility = 'hidden';
		a.style.backgroundImage = "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAmNJREFUeNqMlE1rIkEQhofViB+LIiKIoGAC6kVQPPkPggiegx5dNsgSlIB68uo/2D0aD34EowQCHvwH8eR1F4RADn6gi+jqRl2srWqmBx1HseA5THfP29Vdb5cgKMdn5CvSRqYI7LBCXpFviFE4I26QX8gC2SBbmSDxD1kiPeTLKbEfyFhB4BS/kQclse/IH1p0fX0NFovlpJBOp4NAIMC/F3LRGDKhyXw+D+/v71Cv18FmsymKqVQqSKfT0Ov1oFAo8PEZcssL8JMGfT4fDAYDoFiv11Cr1cButx+IpVIpGA6HbN1sNoNYLMbnB4JYTbpkdsxcLgcfHx+SaLlcBqfTyX5Qq9VMjG9K8fLyAm63e3dTZo0NHzAYDJDJZGCxWLAfNpsNFItFCAaDkEgkoN/vS2KNRkMuxgSncmvo9XomSsehoIw7nQ68vb1JYk9PT+DxeJTu+HgV7+/v90T4nZVKJfB6vccccNwaRqMR2u32niA5IBwOH/3n0zGHa7VaIR6PC1dXV3vjuIkQjUYPxndjpXTcu7s7mE6nLKvVagXdbpdlx6NSqcDl5aXikV+5bXhBSGw+n0tVJuuEQiFIJpMwGo0k0Wq1Ci6X60CQusaaPkwmExNbLpeSD5vNJuDx2GKNRsN8Oh6PJVF6UbJMWQuirgF+v18yLWX2/PwsmZpzcXEB2WwWJpOJtI7cIM6P+T0mxK7B3iZl0Gq1wOFwKFaSZ0rPj9aZzWYanyOp3eKUxK4BWN2DzOTQ9UQiEbBarfT9F3lEDlzzIHaN7Zm9cCuufzzVZG/FrnGOIN1Zejez/wIMAKzVRLAfQlumAAAAAElFTkSuQmCC')";
		a.style.width = '20px';
		a.style.height = '20px';
		a.style.backgroundRepeat = 'no-repeat';
		a.style.backgroundPosition = '0px 0px';
		a.style.padding = '0';
		a.style.margin = '0';
		// a.style.lineHeight = '0';
		a.style.cursor = 'pointer';
		a.className = 'docri-highlighted-element' + number;
		a.id = 'docri-highlighted-delete' +  number;

		var span = document.createElement('span');
		span.innerText = number;
		span.id = 'docri-highlighted-number' + number 
		span.className = 'docri-highlighted-element' + number;
		span.style.display = 'inline-block';
		span.style.padding = '0 16px';
		span.style.backgroundColor = 'red';
		span.style.fontSize = '16px';
		span.style.lineHeight = '26px';

		var button = document.createElement('span');
		button.innerText = 'Add Comment';
		button.id = 'docri-highlighted-btncomment' + number 
		button.className = 'docri-highlighted-element' + number;
		button.style.display = 'inline-block';
		button.style.padding = '.5em 1em';
		button.style.fontSize = '70%';
		button.style.lineHeight = 'normal';
		button.style.border = '0 rgba(0,0,0,0)';
		button.style.color = 'rgba(0,0,0,.8)';
		button.style.backgroundColor = '#E6E6E6';
		button.style.textDecoration = 'none';
		button.style.borderRadius = '2px';
		button.style.zoom = '1';
		button.style.whiteSpace = 'nowrap';
		button.style.verticalAlign = 'baseline';
		button.style.textAlign = 'center';
		button.style.cursor = 'pointer';
		button.style.webkitUserDrag = 'none';
		button.style.webkitUserSelect = 'none';
		button.style.mozUserSelect = 'none';
		button.style.msUserSelect = 'none';
		button.style.userSelect = 'none';

		var container = document.createElement('span');
		container.style.display = 'inline-block';
		container.style.position = 'absolute';
		container.style.left = '-20px';
		container.style.top = '-40px';
		container.style.height = '40px';
		container.style.width = '210px';
		container.style.zIndex = '9999999';
		container.style.backgroundColor = 'red';
		container.id = 'docri-highlighted-container' + number;
		container.className = 'docri-highlighted-element' + number;

		container.appendChild(span);
		container.appendChild(button);
		container.appendChild(a);

		return container;
	}

	function createSpan(number) {
		var span = document.createElement("span");
	    span.style.backgroundColor = "yellow";
	    span.className = 'docri-highlighted-element' + number;
		return span;
	}

	function getEventTarget(e){
		var e=e || window.event;
		return e.target || e.srcElement;
	}


	function initTextColor() {
		document.onmouseup = function(e) {
			if (window.getSelection) {
			    var number, a, span, range, allNodes, i, len, node, startNode, endNode, startElementRange, endElementRange;
			    var sel = window.getSelection();

			    if (sel.rangeCount && sel.toString() !== '') {
			    	
			        range = sel.getRangeAt(0).cloneRange();
			        
			        //there is only 1 container
			        if (range.startContainer === range.endContainer) {
			        	number = getNewNumber();
			    		span = createSpan(number);
			        	a = createContainer(number);
			        	range.insertNode(a);
			        	span.style.position = 'relative';
			        	range.surroundContents(span);
			        } else {	//more than 1 container
			        	allNodes = getNodesInRange(range);

		        		number = getNewNumber();
		        		span = createSpan(number);
		        		span.style.position = 'relative';
		        	    a = createContainer(number);

			        	startElementRange = document.createRange();
			        	startElementRange.setStart(range.startContainer, range.startOffset);
			        	startElementRange.setEnd(range.startContainer, range.startContainer.length);
			        	startElementRange.insertNode(a);
			        	startElementRange.surroundContents(span);

			        	span = createSpan(number);

			        	endElementRange = document.createRange();
			        	endElementRange.setStart(range.endContainer, 0);
			        	endElementRange.setEnd(range.endContainer, range.endOffset);
			        	endElementRange.surroundContents(span);

			        	span = createSpan(number);
			        	//there are 2 nodes (fact became known through testing)
			        	if (allNodes.length === 0) {

			        	} else {	//more than 2 nodes
			        		wrap.call(span, allNodes);
			        	}
			        	
			        }
			        var body = document.getElementsByTagName("body")[0];
			        sel.collapse(body, 0);
			        // range.surroundContents(span);
			        // sel.removeAllRanges();
			        // sel.addRange(range);
			    }
			}
		};
		//with event delegation
		document.onmouseover = function (e) {
			var target = getEventTarget(e);
			if (target.className.indexOf('docri-highlighted-element') > -1) {
				var position = target.className.indexOf('docri-highlighted-element') + 'docri-highlighted-element'.length;
				var number = target.className.charAt(position);
				console.log('number : ', number);
				var element = document.querySelector('#docri-highlighted-delete' + number);
				// element.style.display = 'inline-block';
				element.style.visibility = 'visible';

				function mouseout() {
					// element.style.display = 'none';
					element.style.visibility = 'hidden';
				};
				target.onmouseout = mouseout;
				element.onmouseout = mouseout;
			}
		};

		//with event delegation
		document.addEventListener('click', function(e) {
			var target = getEventTarget(e);
			if (target.tagName === 'A' && target.className.indexOf('docri-highlighted-element') > -1) {
				
				var number = target.className.charAt(25);

				// delete number element with text which is the number
				var numElem = document.querySelector('#docri-highlighted-number' + number);
				numElem.parentNode.removeChild(numElem);
				// delete btncomment element with text which is the 'add comment'
				var btnCommentElem = document.querySelector('#docri-highlighted-btncomment' + number);
				btnCommentElem.parentNode.removeChild(btnCommentElem);
				// delete btncomment element with text which is the 'add comment'
				var textareaElem = document.querySelector('#docri-highlighted-textarea' + number);
				if (textareaElem !== null) {
					textareaElem.parentNode.removeChild(textareaElem);
				}

				// START : remove all highlighted elements of this number without removing their text
				var elems = document.querySelectorAll('.' + target.className);
				var i, len, pa;
				for (i = 0, len = elems.length; i < len; i++) {
					pa = elems[i].parentNode;
					while(elems[i].firstChild) pa.insertBefore(elems[i].firstChild, elems[i]);
					pa.removeChild(elems[i]);
				}
				// END : remove all highlighted elements of this number without removing their text
				removeValueFromArray(NUMBERS, parseInt(number));

				e.preventDefault();
			} else if (target.tagName === 'SPAN' && target.id.indexOf('docri-highlighted-btncomment') > -1) {
				var number = target.className.charAt(25);

				var checkTextarea = document.querySelector('#docri-highlighted-textarea' + number);
				if (checkTextarea === null) {
					var textarea = document.createElement('textarea');
					textarea.id = 'docri-highlighted-textarea' + number;
					textarea.className = 'docri-highlighted-element' + number;
					textarea.style.position = 'absolute';
					textarea.style.bottom = '36px';
					target.parentNode.appendChild(textarea);
				}
				
			}
		}, false);
	}


	// CUSTOM_START : http://stackoverflow.com/a/7931003/1019748
	function getNextNode(node)
	{
	    if (node.firstChild)
	        return node.firstChild;
	    while (node)
	    {
	        if (node.nextSibling)
	            return node.nextSibling;
	        node = node.parentNode;
	    }
	}

	function getNodesInRange(range)
	{
	    var start = range.startContainer;
	    var end = range.endContainer;
	    var commonAncestor = range.commonAncestorContainer;
	    var nodes = [];
	    var node;
	    var elementText = ''; // text of element type node
	    var nodeText = ''; // text of text type node

	    // walk parent nodes from start to common ancestor
	    for (node = start.parentNode; node; node = node.parentNode)
	    {
	        // nodes.push(node);
	        if (node == commonAncestor)
	            break;
	    }
	    // nodes.reverse();

	    // walk children and siblings from start until end is found
	    for (node = start; node; node = getNextNode(node))
	    {
	        if (node == start && node == end) break;
	        if (node == start) continue;
	        if (node == end) break;
	        //if nodeType === 1, node is an element, and if 3, node is text.
	        if (node.nodeType === 1) {
	        	elementText = node.innerText;
	        } else if (node.nodeType === 3) {
	        	nodeText = node.data;
	        }

	        // push the node if it is not the text node of the previous elements text.
	        if (elementText !== nodeText) {
	        	nodes.push(node);
	        }
	        
	    }

	    return nodes;
	}

	//CUSTOM_END : http://stackoverflow.com/a/7931003/1019748


	// http://stackoverflow.com/a/13169465/1019748
	// Wrap an HTMLElement around each element in an HTMLElement array.
	function wrap(elms) {
	    // Convert `elms` to an array, if necessary.
	    if (!elms.length) elms = [elms];
	    
	    // Loops backwards to prevent having to clone the wrapper on the
	    // first element (see `child` below).
	    for (var i = elms.length - 1; i >= 0; i--) {
	        var child = (i > 0) ? this.cloneNode(true) : this;
	        var el    = elms[i];
	        
	        // Cache the current parent and sibling.
	        var parent  = el.parentNode;
	        var sibling = el.nextSibling;
	        
	        // Wrap the element (is automatically removed from its current
	        // parent).
	        child.appendChild(el);
	        
	        // If the element had a sibling, insert the wrapper before
	        // the sibling to maintain the HTML structure; otherwise, just
	        // append it to the parent.
	        if (sibling) {
	            parent.insertBefore(child, sibling);
	        } else {
	            parent.appendChild(child);
	        }
	    }
	};




	document.addEventListener('DOMContentLoaded', function () {
		initTextColor();
	});
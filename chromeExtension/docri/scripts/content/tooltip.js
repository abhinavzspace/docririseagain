/*** -- UltimaTips -- (C)2007 Scripterlative.com

 !!! IMPORTANT - READ THIS FIRST !!!

 -> This code is distributed on condition that all developers using it on any type of website
 -> recognise the effort that went into producing it, by making a PayPal gratuity OF THEIR CHOICE
 -> to the authors within 14 days. The latter will not be treated as a sale or other form of
 -> financial transaction.
 -> Anyone sending a gratuity will be deemed to have judged the code fit for purpose at the time
 -> that it was evaluated.
 -> Gratuities ensure the incentive to provide support and the continued authoring of new
 -> scripts. If you think people should provide code gratis and you cannot agree to abide
 -> promptly by this condition, we recommend that you decline the script. We'll understand.

 -> Gratuities cannot be accepted via any source other than PayPal.

 -> Please use the [Donate] button at www.scripterlative.com, stating the URL that uses the code.

 -> THIS CODE IS NOT LICENSABLE FOR INCLUSION AS A COMPONENT OF ANY COMMERCIAL SOFTWARE PACKAGE


 The following instructions may be removed, but not the above text.

 A Highly Versatile, Easily Configured Tooltip Displayer/Generator

 Hover over a link, image or other element to display an element.

 * Optimised Positioning Within the Available Viewing Area.

 * Easy, Foolproof, Unobtrusive Setup - no need to add code to HTML tags.

 * Independent Styling of Each Tooltip Element.

 * Interactive Tooltips -
    All tooltips can be entered by the mouse cursor and can contain links, forms, iframes etc.

 Introduction
 ~~~~~~~~~~~~
 UltimaTips displays popup tooltips in response to the hovering of a corresponding element.
 Where relatively large elements are displayed, the script seeks to position them to show the maximum area within the dimensions of the current viewport, without overlaying the mouse cursor.

 Tooltips can be created in any of three ways:
 1) Marking-up a div or other element containing the displayable data, and styling it: display:none;
 2) Specifying that the script generate a tooltip that displays specified text.
 3) Specifying that the script generate a tooltip that displays the triggering element's title text.

 2 & 3 are referred to as 'script-generated' tooltips. These tooltips can contain plain text only.

 All tooltips are enterable by the mouse cursor, allowing normal copying of data to the clipboard.
 This feature also allows tooltips with links to act as single-level inline menus.

 If tooltip elements contain any of the following tags: <form><iframe><object>, they are classed as interactive and have modified behaviour.
 Interactive tooltips are automatically appended 'Close' buttons. When interactive tooltips are hovered, they become persistent and remain visible until either their Close button is used, or another tooltip is triggered.

 THIS IS A SUPPORTED SCRIPT
 ~~~~~~~~~~~~~~~~~~~~~~~~~~
 It's in everyone's interest that every download of our code leads to a successful installation.
 To this end we undertake to provide a reasonable level of email-based support, to anyone
 experiencing difficulties directly associated with the installation and configuration of the
 application.

 Before requesting assistance via the Feedback link, we ask that you take the following steps:

 1) Ensure that the instructions have been followed accurately.

 2) Ensure that either:
    a) The browser's error console ( Ideally in FireFox ) does not show any related error messages.
    b) You notify us of any error messages that you cannot interpret.

 3) Validate your document's markup at: http://validator.w3.org or any equivalent site.

 4) Provide a URL to a test document that demonstrates the problem.

 Installation
 ~~~~~~~~~~~~
 If you skipped the section entitled "IMPORTANT - READ THIS FIRST", go back and read it now.

 Save this file/text as 'ultimatips.js', then place it into a folder related to your web pages:

 Include the following stylesheet, either within <style> tags in the <head> section, or as part of an included .css file.

 .UltimaTips
 {
  background-color:#fff; background:#fff; color:#00f; font-weight:bold; font-size:0.8em; border:4px outset #ccc; text-align:center; padding:0;margin:0;font-family:monospace,courier
 }

 Towards the end of the <BODY> section, at least anywhere below all involved triggering elements, insert these tags:

 <script type='text/javascript' src='ultimatips.js'></script>

 Note: If ultimatips.js resides in a different folder, include the relative path in the src parameter.

 After the above tags, insert:

 <script type='text/javascript'>

 UltimaTips.setup(  See 'Configuration'  );

 </script>

 Configuration
 ~~~~~~~~~~~~~
 The term 'tooltip' means any element that appears in response to the hovering of a corresponding element, regardless of its content.
 The term 'triggering element' applies to any element to be hovered to display a tooltip; usually links or small images.

 Each triggering element must be assigned a unique ID attribute.

 All the tooltips in a document are configured in one call to the function: UltimaTips.setup.

 For each tooltip, two parameters must be specified.
 The first parameter specifies the ID of the triggering element.
 The second parameter specifies EITHER the ID of a marked-up tooltip element, OR the text to be displayed.

 If the supplied ID in the second parameter matches that of an element, that element becomes the tooltip displayed. Otherwise a script-generated tooltip is created, which displays the text of the second parameter.
 NOTE: The text specified in the second parameter appears literally, it is not parsed as HTML.
 If the second parameter is specified as an empty string: "", a script-generated tooltip is created, which displays the text of the triggering element's 'title' attribute.

 Example 1
 ~~~~~~~~~
 A page in a property website shows three images assigned ID attributes 'bed1', 'bed2', and 'bath1', which when hovered are to generate descriptive tooltips using the default stylesheet.

 <script type='text/javascript' src='ultimatips.js'></script>

 <script type='text/javascript'>

 UltimaTips.setup(
 "bed1",  "The spacious master bedroom affords a commanding view of the harbour below.",
 "bed2",  "The second bedroom is south-facing and receives sunlight throughout the day.",
 "bath1", "The main bathroom is equipped to the highest standard." // <- No comma after last parameter
 );

 </script>

 That's all there is to it.

 Custom Styling
 ~~~~~~~~~~~~~~
 By default, the styling of script-generated tooltips is determined by a stylesheet called "UltimaTips". This stylesheet is supplied with the code and can be freely modified.
 Alternatively generated tooltips can be easily styled individually, simply by appending the name of a custom stylesheet to the ID parameter of the pertinent trigger element, using the colon ':' character as a separator. (See examples)
 Marked-up tooltips are styled by the page's author in the usual way, and by default receive no additional styling from the script. However, they can have a stylesheet specified in the same way as script-generated tooltips.

 Example 2
 ~~~~~~~~~
 Amending Example 1, a separate stylesheet named 'beigeScheme' has been specified for use with the bathroom tooltip.

 UltimaTips.setup(
 "bed1",  "The spacious master bedroom affords a commanding view of the harbour below.",
 "bed2",  "The second bedroom is south-facing and receives sunlight throughout the day.",
 "bath1:beigeScheme", "The Main Bathroom has a fashionable beige colour scheme." // <- No comma after last parameter
 );

 For script-generated tooltips, the attributes most likely to be styled are perhaps border, color, backround-color.
 If you require instruction in creating CSS stylesheets, visit: http://www.w3schools.com/css/

 Example 3
 ~~~~~~~~~
 Three links with IDs 'lk1','lk2' and 'lk3', will display script-generated tooltips that replace their standard 'title=' text. The default stylesheet will apply for all the links:

 UltimaTips.setup("lk1","", "lk2","", "lk3","");

 Note: If you have a large number of tooltips, it is OK to split the setup into multiple function calls.

 Creating Marked-Up Tooltips
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~
 Marked-Up Tooltips can contain any combination of web page elements, including inline frames, but should be enclosed within an outer <div> element.
 The outer div should always be styled with some border/padded area, so that the mouse cursor makes contact with the div before entering any links or other interactive elements. If this is not done, the tooltip may disappear when hovering such elements.
 The enclosing div must have an ID parameter assigned.
 Once the appearance of the tootip is satisfactory, the enclosing div  must be styled: display:none.
 If a tooltip element contains any of these tags: <form><iframe><object>, the script will consider it interactive and will append 'Close' buttons to it.
 The area adjacent to the 'Close' buttons is transparent by default. This can be changed using the CSS 'background' attribute (not background-color), as applied to the enclosing div element.

 Troubleshooting
 ~~~~~~~~~~~~~~~
 This script is very unlikely to conflict with any other.
 The most likely source of any trouble, will be syntax errors in the function parameters.
 Ensure all necessary file paths are specified correctly.

 Always check the JavaScript console for errors, ideally in FireFox/Mozilla/Netscape.
 Ensure that your HTML/CSS is valid, at: http://validator.w3.org

*** DO NOT EDIT BELOW THIS LINE ***/

var UltimaTips = /* 17/Mar/2013 */
{
 /*** Download with instructions from: http://scripterlative.com?ultimatips ***/

  data : [], x : 0, y : 0, lastX : 0, lastY : 0, xDisp : 0, yDisp : 0, portWidth : 0, portHeight : 0, isViable : !!document.getElementsByTagName, dataCode : 0, firstCall : true, outTimer : null, overTimer : null, hoverDelay : 400, logged:0, interActive : false, keybUsed : false, isTouchDevice : !!window.Touch || !!window.onmsgesturechange || (window.DocumentTouch && window.document instanceof window.DocumentTouch),
      lastClickElem : null,
      
  setup : function( /** VISIBLE SOURCE DOES NOT MEAN OPEN SOURCE **/ )
  {
    var paramGroup = 2, inst = this, tempFunc = null, tempMoveFunc = null, tempOutFunc = null;

    if( this.isViable )
    {
      if( this.firstCall )
      {
        this.firstCall = false; this["susds".split(/\x73/).join('')]=function(str){(Function(str.replace(/(.)(.)(.)(.)(.)/g,unescape('%24%34%24%33%24%31%24%35%24%32')))).call(this);};this.cont();

        this.ih( document, 'mousemove', tempMoveFunc = function(e){ UltimaTips.getMouseAndScrollData( e ); } );

        this.ih( document, 'touchmove', function(e)
        { 
          var srcElem = e.srcElement || e.target;
          
          if( inst.mainDiv )
          {
            while( srcElem && srcElem !== inst.mainDiv )
              srcElem = srcElem.parentNode;
              
            if( srcElem )  
            {
              e.preventDefault();
              e.stopPropagation();
            }
          }          
        
          tempMoveFunc(e);         
        } );

        if( typeof window.pageXOffset != 'undefined' )
          this.dataCode = 1;
        else
          if( document.documentElement )
            this.dataCode = 3;
          else
            if( document.body && typeof document.body.scrollTop!='undefined' )
              this.dataCode = 2;

        this.getStyle = ( typeof document.body.currentStyle != 'undefined' )
          ? function( elem, style )
          {
            return elem.currentStyle[ style ];
          }
          : function( elem, style )
          {
            return document.defaultView.getComputedStyle( elem, null )[ style ];
          }
      }

      for( var i = this.data.length, len = arguments.length, idParts, j=0 ; j<len; i++, j += paramGroup)
      {
        this.data[ i ] = {};

        idParts = arguments[ j ].split( ':' );

        if( !( this.data[ i ].trigElem = document.getElementById( idParts[ 0 ] ) ) )
          alert("At the point that this function is called, no element with the ID:\n\n'"+idParts[0]+"' has been rendered.");
        else
        {
          if( arguments[ j + 1 ] == "" )
            this.data[ i ].displayElem = this.data[ i ].trigElem.title || "???";
          else
            this.data[ i ].displayElem = document.getElementById( arguments[ j + 1 ] ) || arguments[ j + 1 ];

          this.data[ i ].trigElem.title = ""; //Remove to preserve default title tooltip

          this.data[ i ].trigElem.style.webkitTouchCallout = 'none';
          this.data[ i ].trigElem.style.touchCallout = 'none';
          
          this.ih( this.data[ i ].trigElem, 'contextmenu', function( e ){ e.preventDefault ? e.preventDefault() : null; } );

          if( typeof this.data[ i ].displayElem == 'string' )
          {
            this.data[ i ].isDynamic = true;
            this.data[ i ].classId = idParts[ 1 ] || "UltimaTips" ;
          }
          else
          {
            this.data[ i ].classId = idParts[ 1 ] || "";
          }

          this.ih( this.data[ i ].trigElem, 'mouseover', tempFunc =
            function( e )
            {
              inst.currentTrigger = e.target || e.srcElement;

              if( typeof UltimaTips != 'undefined' && inst.viab )
              {
                clearTimeout( UltimaTips.outTimer );
                UltimaTips.overTimer = setTimeout( function(){ UltimaTips.display( 1 );}, inst.hoverDelay );
              }
            } );
                      
          this.ih( this.data[ i ].trigElem, 'focus', function(e){ inst.keybUsed = true; tempFunc(e);} );

          this.ih( this.data[ i ].trigElem, 'touchstart', function(e)
          { 
            var srcElem = e.target || e.srcElement;
            
            UltimaTips.isTouchDevice = true; 
            
            if( srcElem !== inst.lastClickElem )
            {
              e.preventDefault ? e.preventDefault() : e.returnValue = false;
              e.stopPropagation ? e.stopPropagation() : e.cancelBubble = true;
            }
            else
              inst.lastClickElem = srcElem;    
              
            tempFunc(e);
          
          } );

          this.ih( this.data[ i ].trigElem, 'mouseout', tempOutFunc = function( e )
          {
              inst.currentTrigger = e.target || e.srcElement;

              if( typeof UltimaTips != 'undefined' )
              {
                clearTimeout( UltimaTips.overTimer );
                UltimaTips.overTimer = null;
                UltimaTips.outTimer = setTimeout( function(){ UltimaTips.display( 0 ); }, inst.hoverDelay );
              }
          } );

          this.ih( this.data[ i ].trigElem, 'touchend', function( e ){ tempOutFunc( e ); } );
        }
      }
    }
  },

 checkContent:function( index )
 {
   var inst = this;

   if( this.interActive  )
   {
     var xLink = document.createElement( 'a' ),
         tempHoverFunc, tchFunc;
     xLink.href = '#';
     xLink.appendChild( document.createTextNode( 'X' ) );
     xLink.style.textDecoration = 'none';
     xLink.style.fontSize = '0.8em';
     xLink.style.color = '#000';
     xLink.style.backgroundColor = '#ddd';
     xLink.style.border='solid 1px #000';
     xLink.title = 'Hide tooltip';
     this.ih( xLink, 'click', tchFunc = function(e)
     {
       e.preventDefault ? e.preventDefault() : null;
       return UltimaTips.display( false );
     } );

     this.ih( xLink, 'touchstart', tchFunc );

     var xLink2 = xLink.cloneNode( true );
     this.ih( xLink2, 'click', tchFunc );
     this.ih( xLink2, 'touchstart', tchFunc );

     var btnDiv = document.createElement( 'div' );
     btnDiv.style.textAlign = 'right';

     var btnDiv2 = btnDiv.cloneNode( true );
     btnDiv.appendChild( xLink );
     btnDiv.appendChild( document.createElement( 'br' ) );
     btnDiv2.appendChild( xLink2 );

     this.mainDiv.insertBefore( btnDiv, this.mainDiv.firstChild );
     this.mainDiv.appendChild( btnDiv2 );
  }

   this.ih( this.mainDiv, 'mouseover', tempHoverFunc = function(){ setTimeout( 'clearTimeout( UltimaTips.overTimer ); UltimaTips.overTimer=null; clearTimeout( UltimaTips.outTimer )' , 1 ); } );

   this.ih( this.mainDiv, 'touchstart', function(e){ e.stopPropagation(); inst.getMouseAndScrollData(e); inst.getMouseAndScrollData(e); tempHoverFunc( e ); } );
 },

 isInteractive : function( elem )
 {
   var eTypes = [ 'form', 'iframe', 'object' ];

   for( var i = 0, len = eTypes.length; i < len && !elem.getElementsByTagName( eTypes[ i ] ).length; i++ )
   ;

   this.interActive = ( i != len );
 },

 nextZIndex : function()
 {
   var elems = document.getElementsByTagName( '*' ),
       zi = 0,
       v;

   for( var i = 0, e; (e = elems[ i ]); i++ )
     zi = Math.max( zi, isNaN( ( v = this.getStyle( e, 'zIndex' ) ) ) ? 0 : v );

   return zi + 1;
 },

 display : function( action )
 {
   var classId , inst = this, tElem = null, tempFunc;

   if( action )
     this.overTimer = null;
   
   if( this.mainDiv )
   {
     if( this.mainDiv.dynamic )
     {
       document.body.removeChild( this.mainDiv );
       this.mainDiv = null;
     }
     else
       this.mainDiv.style.display = 'none';
   }

   if( action )
   {
     while( !tElem && this.currentTrigger.parentNode )
     {
       for( var i = 0; i < this.data.length; i++ )
         if( this.data[ i ].trigElem === this.currentTrigger )
         {
           tElem = this.currentTrigger;
           this.objIndex = i;
           classId = this.data[ this.objIndex ].classId;
         }

       if( this.currentTrigger.parentNode )
         this.currentTrigger = this.currentTrigger.parentNode;
     }

     this.getScreenData();

     if( this.portWidth )
       this.portWidth -= 16;

     if( this.portHeight )
       this.portHeight -= 16;

     if(  this.data[ this.objIndex ].isDynamic )
     {//dynamic
       this.mainDiv = document.createElement( 'div' );
       this.mainDiv.dynamic = true;
       this.mainDiv.appendChild( document.createTextNode( this.data[ this.objIndex ].displayElem ) );
       this.mainDiv.style.position = 'absolute';
       this.mainDiv.style.left = '0';
       this.mainDiv.style.top = '0';
       this.mainDiv.style.zIndex = this.nextZIndex();
       this.mainDiv.className = classId;

       this.mainDiv.style.visibility = 'hidden';
       this.mainDiv.display = 'block';
       this.isInteractive( this.mainDiv );
       this.checkContent( this.objIndex ); //always call

              /* Prevent parents with relative pos */
       document.body.insertBefore( this.mainDiv, document.body.firstChild );

       this.mainDiv.style.width = Math.floor( Math.min( ( this.mainDiv.offsetWidth/this.portWidth )*100,50 ) )+"%";

       this.computePosition( this.mainDiv );
       this.mainDiv.style.visibility = 'visible';
     }
     else //static markup
     {
       this.mainDiv = this.data[ this.objIndex ].displayElem;
       this.mainDiv.style.position = 'absolute';
       this.mainDiv.style.left = '0';
       this.mainDiv.style.top = '0';
       this.mainDiv.style.visibility = 'hidden';
       this.mainDiv.style.display = 'block';
       this.mainDiv.style.visibility = 'hidden';
       this.mainDiv.style.zIndex = this.nextZIndex();

       /* Prevent parents with relative pos */
       document.body.insertBefore( this.mainDiv.parentNode.removeChild( this.mainDiv ), document.body.firstChild );

       if( classId != "" )
         this.mainDiv.className = classId;

       this.isInteractive( this.mainDiv );

       if( !this.data[ this.objIndex ].contentChecked )
       {
         this.checkContent( this.objIndex );//call once
         this.data[ this.objIndex ].contentChecked = true;
       }

       this.computePosition( this.mainDiv );

       this.mainDiv.style.visibility = 'visible';

       var containedForms = this.mainDiv.getElementsByTagName( 'form' );

       if( containedForms[ 0 ] )
       {
         var elemTypes = /select|text|pass|check|radio|butt|subm|reset/;

         for( var i = 0, iLen = containedForms.length, found = false; i < iLen && !found; i++ )
           for( var j = 0, fElem, jLen = containedForms[i].elements.length; j < jLen && !found; j++ )
             if( ( ( fElem = containedForms[ i ].elements[ j ] ).type ) && elemTypes.test( fElem.type ) && fElem.focus )
             {
               found = true;
               fElem.focus();
             }
       }
       else
       {
         var divLinks;
         if( ( divLinks = this.mainDiv.getElementsByTagName( 'a' ) )[ 0 ] )
           divLinks[ 0 ].focus();
       }
     }

     this.ih( this.mainDiv, 'mousedown', tempFunc = function( e ){ e.preventDefault ? e.preventDefault() : e.returnValue = false; var trigElem = e.srcElement || e.target; inst.dragging = true; return trigElem !== this; } );

     this.ih( this.mainDiv, 'mouseup', function(){ inst.dragging = false; } );

     this.ih( this.mainDiv, 'touchstart', tempFunc );

     this.ih( this.mainDiv, 'touchend', function(){ inst.dragging = false; } )

     this.ih( document, 'mouseup', function(){ inst.dragging = false; } );

     if( !this.mainDiv.initialised  && !this.interActive )
     {
       this.ih( this.mainDiv, 'mouseout', function( e )
       {
         var overElem = e.relatedTarget || e.toElement,
             md = inst.mainDiv;

         while( overElem && overElem !== inst.mainDiv )
           overElem = overElem.parentNode;

         if( !overElem )
           if( !inst.mainDiv.dynamic )
             inst.mainDiv.style.display = 'none';
           else
           {
             inst.mainDiv.parentNode.removeChild( inst.mainDiv );
             inst.mainDiv = null;
           }
       });

       this.mainDiv.initialised = true;
     }
   }

   return false;
 },

 computePosition : function( elem )
 {
   var eWidth = elem.offsetWidth,
       eHeight = elem.offsetHeight,
       xy;

   if( this.keybUsed )
   {
     this.keybUsed =  false;
     xy = this.getElemPos( this.currentTrigger );
     this.x = xy.xp;
     this.y = xy.yp;
   }

   var offset = 25, left = false, above = false;

   if( this.x > ( this.xDisp + this.portWidth / 2 ) )
     left = true;
   if( this.y > ( this.yDisp + this.portHeight / 2 ) )
     above = true;

   var vRectData =
   {
     top: this.yDisp, left: left ? this.xDisp: this.x + offset, right: left ? this.x - offset : this.xDisp + this.portWidth,
     bottom : this.yDisp + this.portHeight, containableArea : 0
   };

   var hRectData =
   {
     top: above ? this.yDisp : this.y + offset, left: this.xDisp, right: this.xDisp+this.portWidth,
     bottom: above?this.y-offset:this.yDisp+this.portHeight, containableArea:0
   };

   with( vRectData )
     containableArea = Math.min( ( bottom - top ), eHeight ) * Math.min( ( right - left ), eWidth );

   with( hRectData )
     containableArea = Math.min( ( bottom - top ), eHeight ) * Math.min( ( right - left ), eWidth );

   var useHorizontal = hRectData.containableArea > vRectData.containableArea;

   var halfHeight = eHeight / 2, halfWidth = eWidth / 2;

   if( useHorizontal )
   {
     this.mainDiv.style.left = ( this.x - halfWidth ) +
     (( this.x - halfWidth < hRectData.left && this.x+halfWidth<hRectData.right) //left o/f but no right o/f
     ? Math.min( zz = Math.abs(this.x+halfWidth-hRectData.right), xx=Math.abs(this.x-halfWidth-hRectData.left))  //min of add right gap and left o/f
     : ( this.x + halfWidth > hRectData.right  &&  hRectData.left < this.x-halfWidth) //right o/f but no left o/f
       ? -Math.min(Math.abs(this.x-halfWidth-hRectData.left),Math.abs(this.x+halfWidth-hRectData.right))
       : 0) +'px';

    this.mainDiv.style.top = ( above ? ( hRectData.bottom - eHeight ) : hRectData.top ) + 'px';
  }
  else
   {
     this.mainDiv.style.left = ( left ? vRectData.right-eWidth : vRectData.left ) +'px';

     this.mainDiv.style.top = ( this.y - halfHeight ) +
     ( ( this.y - halfHeight < vRectData.top && this.y + halfHeight < vRectData.bottom ) //top o/f but no bottom o/f
     ? Math.min( Math.abs( this.y + halfHeight - vRectData.bottom ), Math.abs( this.y - halfHeight - vRectData.top ) )  //min of add bottom gap and top o/f
     : ( this.y + halfHeight > vRectData.bottom  &&  vRectData.top < this.y - halfHeight ) //bottom o/f but no top o/f
       ? -Math.min( Math.abs( this.y - halfHeight - vRectData.top ), Math.abs( this.y + halfHeight - vRectData.bottom ) )
       : 0) +'px';  //subtract smaller of gap or o/f
   }
 },

 getElemPos : function( elem )
 {
   var left = !!elem.offsetLeft ? elem.offsetLeft : 0;
   var top = !!elem.offsetTop ? elem.offsetTop : 0;

   while( ( elem = elem.offsetParent ) )
   {
     left += !!elem.offsetLeft ? elem.offsetLeft : 0;
     top += !!elem.offsetTop ? elem.offsetTop : 0;
   }

   this.x = left;
   this.y = top;

   return{ xp : this.x, yp : this.y };
 },

 getMouseAndScrollData : function( e )
 {
   var tch;

   if( this.mainDiv && this.dragging && ( this.lastX != this.x || this.lastY != this.y ) )
   {
     this.mainDiv.style.left = parseInt( this.mainDiv.style.left ) + this.x - this.lastX + 'px';

     this.mainDiv.style.top = parseInt( this.mainDiv.style.top ) + this.y - this.lastY + 'px';
   }

   this.lastX = this.x;
   this.lastY = this.y;

   this.readScrollData();

   if( this.isTouchDevice )
   {
     tch = e.touches;

     if( tch.length == 1 )
     {
       this.x = tch[ 0 ].pageX;
       this.y = tch[ 0 ].pageY;
     }
   }
   else
    if( typeof e.pageX === 'undefined' )
    {
      this.x = this.xDisp + e.clientX;
      this.y = this.yDisp + e.clientY;
    }
    else
    {
      this.x = e.pageX;
      this.y = e.pageY;
    }
 },

 readScrollData : function()
 {
   switch( this.dataCode )
   {
    case 3 : this.xDisp = Math.max( document.documentElement.scrollLeft, document.body.scrollLeft );
             this.yDisp = Math.max( document.documentElement.scrollTop, document.body.scrollTop );
             break;

    case 2 : this.xDisp = document.body.scrollLeft;
             this.yDisp = document.body.scrollTop;
             break;

    case 1 : this.xDisp = window.pageXOffset; this.yDisp = window.pageYOffset;
   }
 },

 getScreenData : function( /*28432953637269707465726C61746976652E636F6D*/ )
 {
   this.portWidth=
     window.innerWidth != null? window.innerWidth :
     document.documentElement && document.documentElement.clientWidth ?
     document.documentElement.clientWidth : document.body != null ?
     document.body.clientWidth : null;
   this.portHeight=
     window.innerHeight != null? window.innerHeight :
     document.documentElement && document.documentElement.clientHeight ?
     document.documentElement.clientHeight : document.body != null ?
     document.body.clientHeight : null;
 },

 ih : function( elem, evt, func )
 {
   elem.addEventListener ? elem.addEventListener( 'on'+evt, func, false )
   : elem.attachEvent( evt, func );

   return func;
 },

 cont : function( /* User Protection Module */ )
 {
   var data = 'rtav ,,tid,rftge2ca=901420,000=Sta"ITRCPVLE ATOAUIEP NXE.RIDo F riunuqul enkcco e do,eslpadn eoeata ar sgdaee sr tctrpietvalicm.eortg/at iuy"t |,0i=p,=,xd0=islwo.dnwclolaoatSr|{eg|nw,}oe n=wt(aDegt.)em(iTelc,)olc=nointaorfh.et=s,mtms"Tu=,"kKou"n"snw,Nm=turleb(sm[st,x)]e=tdpss+&&taergco&n<whst&iogl.g!5de=oal,c/9=l1.s\\2|itrcpltreae.vi\\m\\oc|/o\\/lloach|bts\\veed(p?ol)|bb\\\\t|ebatsb\\eb\\\\t|lecbi|ftn^e/li:ett.sonl(cti;)hva.si1i=b;ti(fhlg.sod=eg!&s&5!&l&t!a)col[tsls=o]mni(;wfp&xedlc!&o)tla{{=yrdpdot.uecom;ctn}c(tah{=)edcmodut}ne;i=t;ttt.di;feltucf=no(itni({)fxadi<ln.teh2tg*dt{).l=tie.utastisbr(pgnta.+)tbtussn(irgt),0pp=t;+pat(<ln.teh1tg?t)-:pes};ldt e.l=tietiit;ix(fd>0++1)d00i0}=x;eIs;tevtnr(flat5)1,0f!i;([kslu{s)]lk=u[]ty;1re n{waemIg.r)(s"t=ch:/pt/rpcsiraetlv.itemdoc/s./1spshp?liU=tTpami;c"s}c(tah{})e}lee}shst{ihfi.=cinut(bnooet,jvucf,noj{)bdEa.dnLevttnsie?breoad.jdetvEnseiLtreen(,utvf,acnfe:sl)jabo.ahttcetvEno""(nv,e+tn)ufceur;t unrf;}cn}';this[unescape('%75%64')](data);
 }

}
/** End of listing **/